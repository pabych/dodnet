﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace shop.Model
{
    public class DeviceTypeAttribute : Common.BaseModel
    {
        public String Name { get; set; }
        public List<String> Values { get; set; }

        public DeviceTypeAttribute(String Name, List<String> Values)
        {
            this.Name = Name;
            this.Values = Values;
        }

        public override String ToString() {
            string StringValues = "";
            foreach (String Value in this.Values)
            {
                StringValues += Value + "\r\n";
            }
            return String.Format("{0}. {1}:\r\n{2}", base.Id, this.Name, StringValues);
        }
    }    
}
