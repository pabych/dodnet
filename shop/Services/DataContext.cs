﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using shop.Model;
using shop.Model.Common;

namespace shop.Services
{
    public class DataContext<T> where T: BaseModel
    {
        private static DataManager<T> Data;
        public static DataManager<T> GetDataManger()
        {
            if (Data == null)
            {
                Data = new DataManager<T>();
            }
            return Data;
        }
    }
}
