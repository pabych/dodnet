﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using shop.Model.Common;

namespace shop.Services
{
    public class DataManager<T> where T : BaseModel
    {
        public List<T> DB = new List<T>();

        public void Create(T Element)
        {
            Element.Id = this.GetLastId();
            DB.Add(Element);
        }

        public string Read(int Id)
        {
            var el = this.DB.FirstOrDefault(item => item.Id == Id);
            if(el == null) {
                return "Entity with Id=" + Id + " is not found";                       
            }
            return el.ToString();            
        }

        public bool Update(int Id, T NewElement)
        {
            var el = this.DB.FirstOrDefault(item => item.Id == Id);
            if (el == null)
            {
                return false;
            }
            DB.Remove(el);
            NewElement.Id = Id;
            DB.Add(NewElement);
            return true;
        }

        public bool Delete(int Id)
        {
            var el = this.DB.FirstOrDefault(item => item.Id == Id);
            if (el == null)
            {
                return false;
            }
            DB.Remove(el);
            return true;
        }

        public void ReadAll()
        {
            foreach (T Element in DB) {
                Console.WriteLine(Element.ToString());
            }
        }

        public T GetById(int Id)
        {
            foreach (T Element in DB)
            {
                if (Element.Id == Id)
                {
                    return Element;
                }
            }
            return null;
        }

        private int GetLastId()
        {
            return DB.Any() ? DB.Max(item => item.Id) + 1 : 1;
        }
    }
}
