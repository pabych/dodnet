﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using shop.Model;
using shop.Services;
using shop.Controllers;

namespace shop
{
    class Program
    {
        static void Main(string[] args)
        {
            //ComputerType
            ComputerType ComputerType1 = new ComputerType("Mainframe");
            ComputerType ComputerType2 = new ComputerType("NoteBook");
            ComputerType ComputerType3 = new ComputerType("NetBook");

            //Computer
            Computer Computer1 = new Computer("Pentium dual core 3300", ComputerType1);
            Computer Computer2 = new Computer("Pentium Core i5", ComputerType2);
            Computer Computer3 = new Computer("Pentium Core i7", ComputerType2);

            //Attribute
            List<String> Resolution = new List<String>();
            Resolution.Add("1600x1200");
            Resolution.Add("1680x1050");
            DeviceTypeAttribute Attribute1 = new DeviceTypeAttribute("Resolution", Resolution);

            List<String> RAM = new List<String>();
            RAM.Add("8GB");
            RAM.Add("16GB");
            RAM.Add("32GB");
            DeviceTypeAttribute Attribute2 = new DeviceTypeAttribute("RAM", RAM);
            
            List<DeviceTypeAttribute> AttributesList = new List<DeviceTypeAttribute>();

            AttributesList.Add(Attribute1);
            AttributesList.Add(Attribute2);

            //DeviceType
            DeviceType DeviceType1 = new DeviceType("Computer", AttributesList);
            DateTime date = new DateTime();

            //Device
            Device device1 = new Device("Intel", DeviceType1, date);

            //Client
            Client Client1 = new Client("Andrew", "Lisovsky", "pabych93@gmail.com", "+375298889680", "pt.Kletskova 19-80");
            Client Client2 = new Client("Pavel", "Kireichuk", "kireichuk@gmail.com", "+12345678900", "pt.Folush 24-144");
            Client Client3 = new Client("Evgeni", "Xoruzhij", "asd@gmail.com", "+12345678900", "pt.Yuzhnaya 11-43");


            //Order
            List<Device> DevicesList = new List<Device>();
            DevicesList.Add(device1);   

            List<Computer> ComputersList = new List<Computer>();
            ComputersList.Add(Computer1);

            Order Order1 = new Order(Client1, DevicesList, ComputersList);

            DataContext<ComputerType>.GetDataManger().Create(ComputerType1);
            DataContext<ComputerType>.GetDataManger().Create(ComputerType2);
            DataContext<ComputerType>.GetDataManger().Create(ComputerType3);

            DataContext<Client>.GetDataManger().Create(Client1);
            DataContext<Client>.GetDataManger().Create(Client2);
            DataContext<Client>.GetDataManger().Create(Client3);

            DataContext<Computer>.GetDataManger().Create(Computer1);
            DataContext<Computer>.GetDataManger().Create(Computer2);
            DataContext<Computer>.GetDataManger().Create(Computer3);

            //Clients.ReadAll();

            ComputerController ComputerController = new ComputerController();
            for (; ; )
            {
                ConsoleHelper.Start();
            }
        }
    }
}
