﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using shop.Model;
using shop.Services;

namespace shop.Controllers
{
    public class ComputerController
    {
        public void indexAction()
        {
            Console.Clear();
            Console.WriteLine("Computers: ");
            DataContext<Computer>.GetDataManger().ReadAll();
            Console.WriteLine("Actions: ");
            Console.WriteLine("1.Create");
            Console.WriteLine("2.Read");
            Console.WriteLine("3.Update");
            Console.WriteLine("4.Delete");
            
            string input = Console.ReadLine();
            switch (input)
            {
                case "1":
                    this.CreateAction();
                    break;
                case "2":
                    this.ReadAction();
                    Console.Read();
                    break;
                case "3":
                    this.UpdateAction();
                    break;
                case "4": 
                    this.DeleteAction();
                    break;
            }                
        }

        public void CreateAction() {
            Console.Write("Enter name: ");
            string Name = Console.ReadLine();
            Console.WriteLine("Select Type: ");
            DataContext<ComputerType>.GetDataManger().ReadAll();
            int Id = Convert.ToInt32(Console.ReadLine());
            ComputerType Type = DataContext<ComputerType>.GetDataManger().GetById(Id);
            if(Type == null) {
                Console.WriteLine("Type not found");
            }
            Computer NewComputer = new Computer(Name, Type);
            DataContext<Computer>.GetDataManger().Create(NewComputer);
            Console.WriteLine("Computer added");
        }

        public void ReadAction()
        {
            Console.WriteLine("Enter Id");
            int Id = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine(DataContext<Computer>.GetDataManger().Read(Id));
        }

        public void UpdateAction()
        {
            Console.Write("Enter name: ");
            string Name = Console.ReadLine();
            Console.WriteLine("Select Type: ");
            DataContext<ComputerType>.GetDataManger().ReadAll();
            int TypeId = Convert.ToInt32(Console.ReadLine());
            ComputerType Type = DataContext<ComputerType>.GetDataManger().GetById(TypeId);
            if(Type == null) {
                Console.WriteLine("Type not found");
            }
            Computer NewComputer = new Computer(Name, Type);
            Console.Write("Enter Computer Id to update: ");
            int Id = Convert.ToInt32(Console.ReadLine());
            if (!DataContext<Computer>.GetDataManger().Update(Id, NewComputer))
            {
                Console.WriteLine("Entity with Id=" + Id + " is not found");
            }
        }

        public void DeleteAction()
        {
            Console.WriteLine("Enter Computers id want to delete:");
            int Id = Convert.ToInt32(Console.ReadLine());
            if (!DataContext<Computer>.GetDataManger().Delete(Id))
            {
                Console.WriteLine("Entity with Id=" + Id + " is not found");
            }
        }
    }
}
