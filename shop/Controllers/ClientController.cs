﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using shop.Model;
using shop.Services;

namespace shop.Controllers
{
    class ClientController
    {
        public void IndexAction()
        {
            Console.Clear();
            Console.WriteLine("Clients: ");
            DataContext<Client>.GetDataManger().ReadAll();
            Console.WriteLine("Actions: ");
            Console.WriteLine("1.Create");
            Console.WriteLine("2.Read");
            Console.WriteLine("3.Update");
            Console.WriteLine("4.Delete");
            
            string input = Console.ReadLine();
            switch (input)
            {
                case "1":
                    this.CreateAction();
                    break;
                case "2":
                    this.ReadAction();
                    Console.Read();
                    break;
                case "3":
                    this.UpdateAction();
                    break;
                case "4": 
                    this.DeleteAction();
                    break;
            }                
        }

        public void CreateAction() {
            Console.Write("Enter firstname: ");
            string FirstName = Console.ReadLine();
            Console.Write("Enter lastname: ");
            string LastName = Console.ReadLine();
            Console.Write("Enter email: ");
            string Email = Console.ReadLine();
            Console.Write("Enter phone: ");
            string Phone = Console.ReadLine();
            Console.Write("Enter Address: ");
            string Address = Console.ReadLine();

            Client NewClient = new Client(FirstName, LastName, Email, Phone, Address);
            DataContext<Client>.GetDataManger().Create(NewClient);
            Console.WriteLine("Client added");
        }

        public void ReadAction()
        {
            Console.WriteLine("Enter Id");
            int Id = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine(DataContext<Client>.GetDataManger().Read(Id));
        }

        public void UpdateAction()
        {
            Console.Write("Enter firstname: ");
            string FirstName = Console.ReadLine();
            Console.Write("Enter lastname: ");
            string LastName = Console.ReadLine();
            Console.Write("Enter email: ");
            string Email = Console.ReadLine();
            Console.Write("Enter phone: ");
            string Phone = Console.ReadLine();
            Console.Write("Enter Address: ");
            string Address = Console.ReadLine();
            Client NewClient = new Client(FirstName, LastName, Email, Phone, Address);
            Console.Write("Enter Computer Id to update: ");
            int Id = Convert.ToInt32(Console.ReadLine());
            if (!DataContext<Client>.GetDataManger().Update(Id, NewClient))
            {
                Console.WriteLine("Entity with Id=" + Id + " is not found");
            }
        }

        public void DeleteAction()
        {
            Console.WriteLine("Enter Computers id want to delete:");
            int Id = Convert.ToInt32(Console.ReadLine());
            if (!DataContext<Client>.GetDataManger().Delete(Id))
            {
                Console.WriteLine("Entity with Id=" + Id + " is not found");
            }
        }
    }
}
