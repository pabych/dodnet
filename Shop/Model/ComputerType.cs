﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace shop.Model
{
    public class ComputerType : Common.BaseModel
    {
        public string Name { get; set; }

        public ComputerType(String Name)
        {
            this.Name = Name;
        }

        public override String ToString() {
            return String.Format("{0}. {1}", base.Id, this.Name);
        }
    }
}
