﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace shop.Model
{
    public class DeviceType : Common.BaseModel
    {
        public string Name { get; set; }
        public List<DeviceTypeAttribute> Attributes { get; set; }

        public DeviceType(string Name, List<DeviceTypeAttribute> Attributes)
        {
            this.Name = Name;
            this.Attributes = Attributes;
        }

        public override String ToString() {
            string ResultAttributes = "";
            foreach(DeviceTypeAttribute attr in this.Attributes) {
                ResultAttributes += attr.Name + ", ";
            }
            return String.Format("{0}. {1}: {2}", base.Id, this.Name, ResultAttributes);
        }
    }


}
