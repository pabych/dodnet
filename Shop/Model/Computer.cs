﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace shop.Model
{
    public class Computer : Common.BaseModel
    {
        public string Name { get; set; }
        public ComputerType Type { get; set; }

        public Computer(string Name, ComputerType Type)
        {
            this.Name = Name;
            this.Type = Type;
        }

        public override String ToString() {
            return String.Format("{0}. {1} {2}", base.Id, this.Name, this.Type.Name);
        }
    }
}
