﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace shop.Model
{
    public class Client : Common.BaseModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }

        public Client(string FirstName, string LastName, string Email, string Phone, string Address)
        {
            this.FirstName = FirstName;
            this.LastName = LastName;
            this.Email = Email;
            this.Phone = Phone;
            this.Address = Address;
        }

        public override String ToString() {
            return String.Format("{0}. {1} {2}, {3}, {4}, {5}", base.Id, this.FirstName, this.LastName, this.Email, this.Phone,this.Address);
        }
    }
}
