﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace shop.Model
{
    public class Device : Common.BaseModel
    {
        public DeviceType Type { get; set; }
        public DateTime DateOut { get; set; }
        public string Producer { get; set; }
        public int Price { get; set; }

        public Device(string Producer, DeviceType Type, DateTime DateOut)
        {
            this.Type = Type;
            this.DateOut = DateOut;
            this.Producer = Producer;
        }

        public override String ToString() {
            return String.Format("{0}. {1} {2} - {3}$", base.Id, this.DateOut.ToString(), this.Producer, this.Price);
        }
    }
}
