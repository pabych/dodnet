﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace shop.Model
{
    public class Order : Common.BaseModel
    {
        public Client Client { get; set; }
        public List<Device> Devices { get; set; }
        public List<Computer> Computers { get; set; }

        public Order(Client Client, List<Device> Devices, List<Computer> Computers)
        {
            this.Client = Client;
            this.Devices = Devices;
            this.Computers = Computers;
        }

        public override String ToString() {
            string AllDevices = "";
            foreach(Device element in this.Devices) {
                AllDevices += " " + element.ToString() + "\r\n";
            }
            return String.Format("{0}. {1} {2}: \r\n{3}", base.Id, this.Client.FirstName, this.Client.LastName, AllDevices);
        }
    }
}   
